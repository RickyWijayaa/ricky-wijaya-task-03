﻿using System;

namespace Day_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Please enter your first name : ");
            string firstName = Console.ReadLine();
            Console.Write("Please enter your last name : ");
            string lastName = Console.ReadLine();
            string fullName = firstName + " " + lastName;

            int age = 0;
            bool answer = true;
            int year = 0;
            bool answerYear = true;
            
            while (answer)
            {
                Console.Write("Please only enter number for age : ");
                answer = int.TryParse(Console.ReadLine(), out age) == false;
            }

            while (answerYear)
            {
                Console.Write("Please only number for year : ");
                answerYear = int.TryParse(Console.ReadLine(), out year) == false;
            }

            for (int i = 2021; i >= year; i--)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("Your name is : " + fullName);
            Console.WriteLine("Your age is : " + age);
            Console.WriteLine("Your year born : " + year);



        }
    }
}
